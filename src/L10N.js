/**
 * @author    Sándor Papp <spapp@spappsite.hu>
 * @copyright 2017
 * @license   http://opensource.org/licenses/GPL-3.0 GNU General Public License v3
 * @package   BrJs
 * @since     2017-07-15
 */

L10N = {
    /**
     * @constant
     */
    LOCAL_EN: 'en',

    /**
     * @constant
     */
    LOCAL_HU: 'hu',

    /**
     * Returns a language which is sopported in the browser
     *
     * @return {String}
     */
    getDefaultLanguage: function L10N_getDefaultLanguage() {
        var me        = this,
            navigator = window.navigator,
            languages = navigator.languages ? navigator.languages : navigator.userLanguage,
            i, language;

        if (!Array.isArray(languages)) {
            languages = [languages];
        }

        for (i = 0; language = languages[i]; i++) {
            if (me.isSupported(language)) {
                return language;
            }
        }

        return me.LOCAL_EN;
    },

    /**
     * Returns TRUE if the `language` is supported
     *
     * @param {String} language
     * @return {boolean}
     */
    isSupported: function L10N_isSupported(language) {
        return !!this.letterMap[language];
    },

    /**
     * Returns a letter map
     *
     * @param {String} language
     * @return {undefined|{}}
     */
    getLetterMap: function L10N_getLetterMap(language) {
        return this.letterMap[language];
    },

    /**
     * @private
     */
    letterMap: {
        en: {},
        hu: {
            ' ': '',
            '-': '',
            'á': 'a',
            'c': 'ca',
            'cs': 'cb',
            'ccs': 'cbcb',
            'd': 'da',
            'dz': 'db',
            'ddz': 'dbdb',
            'dzs': 'dc',
            'ddzs': 'dcdc',
            'é': 'e',
            'g': 'ga',
            'gy': 'gb',
            'ggy': 'gbgb',
            'í': 'i',
            'l': 'la',
            'ly': 'lb',
            'lly': 'lblb',
            'n': 'na',
            'ny': 'nb',
            'nny': 'nbnb',
            'ó': 'o',
            'ö': 'o',
            'ő': 'o',
            's': 'sa',
            'sz': 'sb',
            'ssz': 'sbsb',
            't': 'ta',
            'ty': 'tb',
            'tty': 'tbtb',
            'ú': 'u',
            'ü': 'u',
            'ű': 'u',
            'z': 'za',
            'zs': 'zb',
            'zss': 'zbzb'
        }
    }
};
