/**
 * @author    Sándor Papp <spapp@spappsite.hu>
 * @copyright 2017
 * @license   http://opensource.org/licenses/GPL-3.0 GNU General Public License v3
 * @package   BrJs
 * @since     2017-07-15
 */

/**
 * @constant
 * @type {string}
 */
Array.DIRECTION_DESC = 'desc';

/**
 * @constant
 * @type {string}
 */
Array.DIRECTION_ASC = 'asc';

(function () {
    var DIRECTION_DESC = Array.DIRECTION_DESC;

    /**
     * @typedef {Object} PreparedLetterMap
     * @property {Object} map - a L10N letter map which is ordered by length and contains upper and lower case text
     * @property {RegExp} regexp - map regexp macher
     */

    /**
     * Returns TRUE if the `object` is an Object
     *
     * @private
     * @param {*} object
     * @return {boolean}
     */
    function isObject(object) {
        return '[object Object]' === Object.prototype.toString.call(object);
    }

    /**
     * Returns TRUE if the `value` is a valid Number
     *
     * @private
     * @param {*} value
     * @return {boolean}
     */
    function isNumeric(value) {
        return !isNaN(parseFloat(value)) && isFinite(value);
    }

    /**
     * Returns TRUE if the `value` is a String
     *
     * @private
     * @param {*} value
     * @return {boolean}
     */
    function isString(value) {
        return 'string' === typeof value;
    }

    /**
     * Returns the prepared `letterMap` and its matcher
     *
     * @private
     * @param {Object} letterMap
     * @return {PreparedLetterMap}
     */
    function prepareLetterMap(letterMap) {
        var peparedKeys       = [],
            preparedLetterMap = {},
            keys              = Object.keys(letterMap),
            uppercaseKey, uppercaseValue, key, i;

        for (i = 0; key = keys[i]; i++) {
            uppercaseKey                    = key.charAt(0).toUpperCase() + key.substr(1);
            uppercaseValue                  = letterMap[key].charAt(0).toUpperCase() + letterMap[key].substr(1);
            preparedLetterMap[key]          = letterMap[key];
            preparedLetterMap[uppercaseKey] = uppercaseValue;

            peparedKeys.push(key);
            peparedKeys.push(uppercaseKey);
        }

        peparedKeys = peparedKeys.sort(function (leftKey, rightKey) {
            return rightKey.length - leftKey.length;
        });

        return {
            map: preparedLetterMap,
            regexp: new RegExp(peparedKeys.join('|'), 'g')
        };
    }

    /**
     * Returns prepared text to ordering which respect the national grammar rules
     *
     * @private
     * @param {String} text
     * @param {PreparedLetterMap} preparedLetterMap
     * @return {string}
     */
    function prepareText(text, preparedLetterMap) {
        var map = preparedLetterMap.map;

        return text.replace(preparedLetterMap.regexp, function (w) {
            return map[w];
        });
    }

    /**
     * Compare two string
     *
     * @private
     * @param {number} leftNumber
     * @param {number} rightNumber
     * @return {number}
     */
    function compareNumber(leftNumber, rightNumber) {
        if (leftNumber < rightNumber) {
            return -1;
        } else if (leftNumber > rightNumber) {
            return 1;
        }

        return 0;
    }

    /**
     * Compare two string
     *
     * @private
     * @param {String} leftString
     * @param {String} rightString
     * @param {PreparedLetterMap} [preparedLetterMap]
     * @return {number}
     */
    function compareString(leftString, rightString, preparedLetterMap) {
        var preparedLeftString  = prepareText(leftString, preparedLetterMap),
            preparedRightString = prepareText(rightString, preparedLetterMap),
            response;

        response = compareNumber(preparedLeftString.toLowerCase(), preparedRightString.toLowerCase());

        if (0 === response) {
            response = compareNumber(preparedRightString, preparedLeftString);
        }

        return response;
    }

    /**
     * Compare two item
     *
     * Returns -1 if `leftItem` is less than `rightItem` by some ordering criterion
     * Returns 1 if `leftItem` is greater than `rightItem` by the ordering criterion
     * Returns 0 if `leftItem` must be equal to `rightItem`
     *
     * @private
     * @param {*} leftItem
     * @param {*} rightItem
     * @param {PreparedLetterMap} [preparedLetterMap]
     * @param {String} [propertyName]
     * @return {number}
     */
    function compare(leftItem, rightItem, preparedLetterMap, propertyName) {
        var isLeftItemNumber      = isNumeric(leftItem),
            isRightItemNumber     = isNumeric(rightItem),
            isLeftItemUndefinded  = typeof leftItem === 'undefined',
            isRightItemUndefinded = typeof rightItem === 'undefined';

        if ((isLeftItemNumber && !isRightItemNumber) || (isLeftItemUndefinded && !isRightItemUndefinded)) {
            return -1;
        } else if ((!isLeftItemNumber && isRightItemNumber) || (!isLeftItemUndefinded && isRightItemUndefinded)) {
            return 1;
        } else if (isLeftItemNumber && isRightItemNumber) {
            return compareNumber(leftItem, rightItem);
        } else if (isString(leftItem) && isString(rightItem)) {
            return compareString(leftItem, rightItem, preparedLetterMap);
        } else if (isLeftItemUndefinded && isRightItemUndefinded) {
            return 0;
        }

        if (isObject(leftItem)) {
            leftItem = leftItem[propertyName];
        }

        if (isObject(rightItem)) {
            rightItem = rightItem[propertyName];
        }

        return compare(leftItem, rightItem, preparedLetterMap, propertyName);

    }

    /**
     * Returns national letter map
     *
     * @private
     * @param {String} [language]
     * @return {undefined|PreparedLetterMap}
     */
    function getLetterMap(language) {
        var letterMap;

        if (language && L10N) {
            language  = language && L10N.isSupported(language) ? language : L10N.getDefaultLanguage();
            letterMap = L10N.getLetterMap(language);
        }

        return prepareLetterMap(letterMap || {});
    }

    /**
     * Sorts the elements of an array in place and returns the array.
     *
     * Supports national grammar rules.
     * See {@link L10N}, {@link L10N#isSupported}
     *
     * @param {Array.<T>} list
     * @param {String} [direction=Array.DIRECTION_ASC]
     * @param {String} [language]
     * @param {String} [propertyName]
     * @return {Array.<T>}
     */
    Array.sort = function Array_sort(list, direction, language, propertyName) {
        var isDirectionDesc   = direction === DIRECTION_DESC,
            preparedLetterMap = getLetterMap(language);

        return list.sort(function (leftItem, rightItem) {
            var response = compare(leftItem, rightItem, preparedLetterMap, propertyName);

            if (isDirectionDesc) {
                response = response * -1;
            }

            return response;
        });
    };
}());
