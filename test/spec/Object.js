/**
 * @author    Sándor Papp <spapp@spappsite.hu>
 * @copyright 2017
 * @license   http://opensource.org/licenses/GPL-3.0 GNU General Public License v3
 * @package   BrJs
 * @since     2017-12-28
 */

describe('Object', function () {

    describe('isObject', function () {
        it('should be defined', function () {
            expect(Object.isObject).not.toBeUndefined();
        });

        it('should be a function', function () {
            expect(typeof Object.isObject).toBe('function');
        });

        it('should return TRUE', function () {
            expect(Object.isObject({})).toBe(true);
        });

        it('should return FALSE', function () {
            expect(Object.isObject(0)).toBe(false);
            expect(Object.isObject(123)).toBe(false);
            expect(Object.isObject('sddsds')).toBe(false);
            expect(Object.isObject('')).toBe(false);
            expect(Object.isObject(/[a-z]/)).toBe(false);
            expect(Object.isObject(function () {
            })).toBe(false);
            expect(Object.isObject(new Date())).toBe(false);
            expect(Object.isObject(Date)).toBe(false);
            expect(Object.isObject([])).toBe(false);
            expect(Object.isObject([1, 3])).toBe(false);
        });
    });

    describe('values', function () {
        it('should be defined', function () {
            expect(Object.values).not.toBeUndefined();
        });

        it('should be a function', function () {
            expect(typeof Object.values).toBe('function');
        });

        it('should return ' + JSON.stringify([1, 2, {one: 1}, [1, 2, 3]]), function () {
            var values = [1, 2, {one: 1}, [1, 2, 3]],
                object = {
                    one: 1,
                    two: 2,
                    object: {
                        one: 1
                    },
                    array: [1, 2, 3]
                };

            expect(JSON.stringify(Object.values(object))).toEqual(JSON.stringify(values));
        });

        it('should return an empty array', function () {
            var expected = '[]';

            expect(JSON.stringify(Object.values({}))).toEqual(expected);
            expect(JSON.stringify(Object.values(1))).toEqual(expected);
            expect(JSON.stringify(Object.values(0))).toEqual(expected);
            expect(JSON.stringify(Object.values(''))).toEqual(expected);
            expect(JSON.stringify(Object.values(/[a-z]/))).toEqual(expected);
            expect(JSON.stringify(Object.values(new Date()))).toEqual(expected);
            expect(JSON.stringify(Object.values(Date))).toEqual(expected);
        });

        it('should return string as an array', function () {
            expect(JSON.stringify(Object.values('abc'))).toEqual('["a","b","c"]');
        });

        it('should return an ERROR', function () {
            function errorTest(value) {
                return function () {
                    Object.values(value)
                };
            }

            expect(errorTest()).toThrowError(TypeError);
            expect(errorTest(null)).toThrowError(TypeError);
        });
    });
});
