/**
 * @author    Sándor Papp <spapp@spappsite.hu>
 * @copyright 2017
 * @license   http://opensource.org/licenses/GPL-3.0 GNU General Public License v3
 * @package   BrJs
 * @since     2017-07-12
 */

/**
 * @param hashtable
 * @constructor
 */
function Hashtable(hashtable) {
    hashtable = hashtable || {};
    this.data = {};

    this.add(hashtable);
}

/**
 * @private
 * @type {null|Object}
 */
Hashtable.prototype.data = null;

/**
 * Sorted key list
 *
 * @private
 * @type {String[]}
 */
Hashtable.prototype.dataOrder = null;

/**
 * Returns the specified value
 *
 * @param {String} key
 * @return {*}
 */
Hashtable.prototype.get = function Hashtable_get(key) {
    return this.data[key];
};

/**
 * Adds a new item
 *
 * @param {String|Object} key
 * @param {*} [value]
 * @return {Hashtable}
 */
Hashtable.prototype.add = function Hashtable_add(key, value) {
    var me = this,
        keyName, keyNames, i;

    if ('[object Object]' === Object.prototype.toString.call(key)) {
        keyNames = Object.keys(key);

        for (i = 0; keyName = keyNames[i]; i++) {
            me.add(keyName, key[keyName]);
        }
    } else {
        me.data[key] = value;
    }

    return this;
};

/**
 * Removes the specified items and returns it
 *
 * @param {String} key
 * @return {*}
 */
Hashtable.prototype.remove = function Hashtable_remove(key) {
    var removedItem = this.data[key];

    delete this.data[key];

    return removedItem;
};

/**
 * Executes a provided function once for each item.
 *
 * @param {Function} callback
 * @param {*} [scope]
 */
Hashtable.prototype.each = function Hashtable_each(callback, scope) {
    var me   = this,
        data = me.data,
        keys = me.dataOrder || Object.keys(data),
        key, i;

    scope = scope || me;

    for (i = 0; key = keys[i]; i++) {
        callback.call(scope, data[key], key);
    }
};

/**
 * Sorts by its key
 *
 * If the `propertyName` is specified then will sort by it
 *
 * @param {String} [propertyName]
 * @param {String} [direction]
 * @param {String} [language]
 */
Hashtable.prototype.sort = function Hashtable_sort(propertyName, direction, language) {
    var me   = this,
        keys = Object.keys(me.data),
        dataOrder, i, key, tmpItems, tmpItem, value, valueType;

    if (propertyName) {
        tmpItems = [];

        for (i = 0; key = keys[i]; i++) {
            valueType = typeof me.data[key];

            if (Object.isObject(me.data[key])) {
                value = me.data[key][propertyName];
            } else if (valueType === 'string' || valueType === 'number' || valueType === 'boolean') {
                value = me.data[key];
            } else {
                value = me.data[key] + '';
            }

            tmpItems.push({
                key: key,
                value: value
            });
        }

        tmpItems  = Array.sort(tmpItems, direction, language, 'value');
        dataOrder = [];

        for (i = 0; tmpItem = tmpItems[i]; i++) {
            dataOrder.push(tmpItem.key);
        }

    } else {
        dataOrder = Array.sort(keys, direction, language);
    }

    me.dataOrder = dataOrder;
};
