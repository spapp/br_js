/**
 * @author    Sándor Papp <spapp@spappsite.hu>
 * @copyright 2017
 * @license   http://opensource.org/licenses/GPL-3.0 GNU General Public License v3
 * @package   BrJs
 * @since     2017-07-10
 */

describe('CompanyDetail', function () {
    var textData = 'COM,Company,15',
        instance;
    // beforeAll(function (done) {
    // });

    it('should be defined', function () {
        expect(CompanyDetail).not.toBeUndefined();
    });

    it('should be a function', function () {
        expect(typeof CompanyDetail).toBe('function');
    });

    describe('static', function () {
        describe('GLUE', function () {
            it('should be defined', function () {
                expect(CompanyDetail.GLUE).not.toBeUndefined();
            });

            it('should be a string', function () {
                expect(typeof CompanyDetail.GLUE).toBe('string');
            });

            it('should be ","', function () {
                expect(CompanyDetail.GLUE).toBe(',');
            });
        });

        describe('fromString()', function () {
            it('should be defined', function () {
                expect(CompanyDetail.fromString).not.toBeUndefined();
            });

            it('should be a function', function () {
                expect(typeof CompanyDetail.fromString).toBe('function');
            });

            it('should returns a "CompanyDetail" class', function () {
                expect(CompanyDetail.fromString(textData).__proto__.constructor.name).toBe('CompanyDetail');
            });
        });
    });
    describe('instance', function () {
        beforeAll(function () {
            instance = new CompanyDetail(new Company('Company', 'COM'), 15);
        });

        describe('getCompany()', function () {
            it('should be defined', function () {
                expect(instance.getCompany).not.toBeUndefined();
            });

            it('should be a function', function () {
                expect(typeof instance.getCompany).toBe('function');
            });

            it('should be a Company class', function () {
                expect(instance.getCompany().__proto__.constructor.name).toBe('Company');
            });

            it('name should be "Company"', function () {
                expect(instance.getCompany().getName()).toBe('Company');
            });

            it('abbreviation should be "Company"', function () {
                expect(instance.getCompany().getAbbreviation()).toBe('COM');
            });
        });

        describe('getDetail()', function () {
            it('should be defined', function () {
                expect(instance.getDetail).not.toBeUndefined();
            });

            it('should be a function', function () {
                expect(typeof instance.getDetail).toBe('function');
            });

            it('should returns 15', function () {
                expect(instance.getDetail()).toBe(15);
            });
        });

        describe('toString()', function () {
            it('should be defined', function () {
                expect(instance.toString).not.toBeUndefined();
            });

            it('should be a function', function () {
                expect(typeof instance.toString).toBe('function');
            });

            it('should returns "' + textData + '"', function () {
                expect(instance.toString()).toBe(textData);
            });
        });

        describe('compare()', function () {
            beforeAll(function () {
            });

            it('should be defined', function () {
                expect(instance.compare).not.toBeUndefined();
            });

            it('should be a function', function () {
                expect(typeof instance.compare).toBe('function');
            });

            it('should returns -1', function () {
                var companyDetail = new CompanyDetail(new Company('Company2', 'CO2'), 20);
                expect(instance.compare(companyDetail)).toBe(-1);
            });

            it('should returns 1', function () {
                var companyDetail = new CompanyDetail(new Company('Company2', 'CO2'), 10);
                expect(instance.compare(companyDetail)).toBe(1);
            });

            it('should returns 0', function () {
                var companyDetail = new CompanyDetail(new Company('Company2', 'CO2'), 15);
                expect(instance.compare(companyDetail)).toBe(0);
            });
        });

        describe('setCompany()', function () {
            it('should be defined', function () {
                expect(instance.setCompany).not.toBeUndefined();
            });

            it('should be a function', function () {
                expect(typeof instance.setCompany).toBe('function');
            });

            it('should returns name="Other Company" and abbreviation="OCO"', function () {
                instance.setCompany(new Company('Other Company', 'OCO'));

                expect(instance.getCompany().getName()).toBe('Other Company');
                expect(instance.getCompany().getAbbreviation()).toBe('OCO');
            });
        });

        describe('setDetail()', function () {
            it('should be defined', function () {
                expect(instance.setDetail).not.toBeUndefined();
            });

            it('should be a function', function () {
                expect(typeof instance.setDetail).toBe('function');
            });

            it('should returns 20', function () {
                instance.setDetail(20);
                expect(instance.getDetail()).toBe(20);
            });
        });
    });
});
