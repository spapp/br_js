/**
 * @author    Sándor Papp <spapp@spappsite.hu>
 * @copyright 2017
 * @license   http://opensource.org/licenses/GPL-3.0 GNU General Public License v3
 * @package   BrJs
 * @since     2017-07-10
 */

/**
 * @param {String} name
 * @param {String} abbreviation
 * @constructor
 */
function Company(name, abbreviation) {
    this.setName(name);
    this.setAbbreviation(abbreviation);
}

/**
 * Name of company.
 *
 * @type {String}
 * @private
 */
Company.prototype.name = undefined;

/**
 * Abbreviation of company.
 *
 * @type {String}
 * @private
 */
Company.prototype.abbreviation = undefined;

/**
 * Returns name of company
 *
 * @return {String}
 */
Company.prototype.getName = function () {
    return this.name;
};

/**
 * Sets name of company.
 *
 * @param {String} name
 * @return {Company}
 */
Company.prototype.setName = function (name) {
    this.name = name;

    return this;
};

/**
 * Returns abbreviation of company
 *
 * @return {String}
 */
Company.prototype.getAbbreviation = function () {
    return this.abbreviation;
};

/**
 * Sets abbreviation of company.
 *
 * @param {String} abbreviation
 * @return {Company}
 */
Company.prototype.setAbbreviation = function (abbreviation) {
    this.abbreviation = abbreviation;

    return this;
};
