/**
 * @author    Sándor Papp <spapp@spappsite.hu>
 * @copyright 2017
 * @license   http://opensource.org/licenses/GPL-3.0 GNU General Public License v3
 * @package   BrJs
 * @since     2017-07-11
 */

/**
 * @param {Company} company
 * @param {Number} detail
 * @constructor
 */
function CompanyDetail(company, detail) {
    this.setCompany(company);
    this.setDetail(detail);
}

/**
 * Glue of string representation
 *
 * @constant
 * @type {string}
 */
CompanyDetail.GLUE = ',';

/**
 * Factory method
 *
 * Returns a {@link CompanyDetail} class from a string.
 * See {@link CompanyDetail#toString}
 *
 * @param {String} companyDetailText
 * @return {CompanyDetail}
 */
CompanyDetail.fromString = function CompanyDetail_fromString(companyDetailText) {
    var parts               = companyDetailText.split(CompanyDetail.GLUE),
        companyName         = parts[0],
        companyAbbreviation = parts[1],
        companyDetail       = parts[2];

    return new CompanyDetail(new Company(companyName, companyAbbreviation), companyDetail);
};

/**
 * Company
 *
 * @type {Company}
 */
CompanyDetail.prototype.company = undefined;

/**
 * Detail of company
 *
 * @type {Number}
 */
CompanyDetail.prototype.detail = undefined;

/**
 * Returns {@link CompanyDetail} as a string
 *
 * The string representation:
 *      <company abbreviation>GLUE<company name>GLUE<detail>
 *
 * @example
 * <code>
 *     var company       = new Company('Company', 'COM'),
 *         companyDetail = new CompanyDetail(company, 15);
 *
 *     companyDetail.toString(); // 'COM,Company,15'
 * </code>
 * @return {string}
 */
CompanyDetail.prototype.toString = function CompanyDetail_toString() {
    var me      = this,
        company = me.getCompany(),
        response;

    response = [
        company.getAbbreviation(),
        company.getName(),
        me.getDetail()
    ].join(CompanyDetail.GLUE);

    return response;
};

/**
 * Compare two {@link CompanyDetail#detail}
 *
 * Returns means:
 *       1 - instance is bigger
 *      -1 - the given `companyDetail` is bigger
 *       0 - equal
 *
 * @param {CompanyDetail} companyDetail
 * @return {number}
 */
CompanyDetail.prototype.compare = function CompanyDetail_compare(companyDetail) {
    var me          = this,
        response    = 0,
        meDetail    = me.getDetail(),
        otherDetail = companyDetail.getDetail();

    if (meDetail > otherDetail) {
        response = 1;
    } else if (meDetail < otherDetail) {
        response = -1;
    }

    return response;
};

/**
 * Returns company
 *
 * @return {Company}
 */
CompanyDetail.prototype.getCompany = function CompanyDetail_getCompany() {
    return this.company;
};

/**
 * Sets up company
 *
 * @param {Company} company
 * @return {CompanyDetail}
 */
CompanyDetail.prototype.setCompany = function CompanyDetail_setCompany(company) {
    this.company = company;

    return this;
};

/**
 * Returns detail
 *
 * @return {Number}
 */
CompanyDetail.prototype.getDetail = function CompanyDetail_getDetail() {
    return this.detail;
};

/**
 * Sets up detail
 *
 * @param {Number} detail
 * @return {CompanyDetail}
 */
CompanyDetail.prototype.setDetail = function CompanyDetail_setDetail(detail) {
    this.detail = detail;

    return this;
};
