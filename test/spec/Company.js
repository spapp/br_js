/**
 * @author    Sándor Papp <spapp@spappsite.hu>
 * @copyright 2017
 * @license   http://opensource.org/licenses/GPL-3.0 GNU General Public License v3
 * @package   BrJs
 * @since     2017-07-10
 */

describe('Company', function () {
    var instance;
    // beforeAll(function (done) {
    // });

    it('should be defined', function () {
        expect(Company).not.toBeUndefined();
    });

    it('should be a function', function () {
        expect(typeof Company).toBe('function');
    });

    describe('instance', function () {
        beforeAll(function () {
            instance = new Company('Company', 'COM');
        });

        describe('getName()', function () {
            it('should be defined', function () {
                expect(instance.getName).not.toBeUndefined();
            });

            it('should be a function', function () {
                expect(typeof instance.getName).toBe('function');
            });

            it('should returns "Company"', function () {
                expect(instance.getName()).toBe('Company');
            });
        });

        describe('getAbbreviation()', function () {
            it('should be defined', function () {
                expect(instance.getAbbreviation).not.toBeUndefined();
            });

            it('should be a function', function () {
                expect(typeof instance.getAbbreviation).toBe('function');
            });

            it('should returns "Company"', function () {
                expect(instance.getAbbreviation()).toBe('COM');
            });
        });

        describe('setName()', function () {
            it('should be defined', function () {
                expect(instance.setName).not.toBeUndefined();
            });

            it('should be a function', function () {
                expect(typeof instance.setName).toBe('function');
            });

            it('should returns "Other Company"', function () {
                instance.setName('Other Company');
                expect(instance.getName()).toBe('Other Company');
            });
        });

        describe('setAbbreviation()', function () {
            it('should be defined', function () {
                expect(instance.setAbbreviation).not.toBeUndefined();
            });

            it('should be a function', function () {
                expect(typeof instance.setAbbreviation).toBe('function');
            });

            it('should returns "OCO"', function () {
                instance.setAbbreviation('OCO');
                expect(instance.getAbbreviation()).toBe('OCO');
            });
        });
    });
});

