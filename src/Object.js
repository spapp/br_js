/**
 * @author    Sándor Papp <spapp@spappsite.hu>
 * @copyright 2017
 * @license   http://opensource.org/licenses/GPL-3.0 GNU General Public License v3
 * @package   BrJs
 * @since     2017-07-22
 */

if (!Object.values) {
    /**
     * Polyfill
     * Returns an array of a given object's own enumerable property values.
     *
     * @param {Object} object
     * @return {Array}
     */
    Object.values = function Object_values(object) {
        var keys     = Object.keys(object),
            keyCount = keys.length,
            values   = [],
            i;

        for (i = 0; i < keyCount; i++) {
            values.push(object[keys[i]]);
        }

        return values;
    }
}

if (!Object.isObject) {
    /**
     * Returns TRUE if the given object is an Object
     *
     *  @param {*} object
     * @return {boolean}
     */
    Object.isObject = function Object_isObject(object) {
        return '[object Object]' === Object.prototype.toString.call(object);
    }
}