/**
 * @author    Sándor Papp <spapp@spappsite.hu>
 * @copyright 2017
 * @license   http://opensource.org/licenses/GPL-3.0 GNU General Public License v3
 * @package   BrJs
 * @since     2017-07-11
 */

describe('Hashtable', function () {
    var instance;

    it('should be defined', function () {
        expect(Hashtable).not.toBeUndefined();
    });

    it('should be a function', function () {
        expect(typeof Hashtable).toBe('function');
    });

    describe('instance', function () {
        beforeAll(function () {
            instance = new Hashtable({
                one: 'one value',
                two: 'two value'
            });
        });

        describe('get()', function () {
            it('should be defined', function () {
                expect(instance.get).not.toBeUndefined();
            });

            it('should be a function', function () {
                expect(typeof instance.get).toBe('function');
            });

            it('returns "one value"', function () {
                expect(instance.get('one')).toBe('one value');
            });

            it('returns "two value"', function () {
                expect(instance.get('two')).toBe('two value');
            });

            it('returns undefined', function () {
                expect(instance.get('three')).toBeUndefined();
            });
        });

        describe('add()', function () {
            it('should be defined', function () {
                expect(instance.add).not.toBeUndefined();
            });

            it('should be a function', function () {
                expect(typeof instance.add).toBe('function');
            });

            describe('single item', function () {
                it('returns "single value"', function () {
                    instance.add('single', 'single value');
                    expect(instance.get('single')).toBe('single value');
                });
            });

            describe('multiple items', function () {
                beforeAll(function () {
                    instance.add({
                        'multiple1': 'multiple1 value',
                        'multiple2': 'multiple2 value'
                    });
                });

                it('returns "multiple1 value"', function () {
                    expect(instance.get('multiple1')).toBe('multiple1 value');
                });

                it('returns "multiple2 value"', function () {
                    expect(instance.get('multiple2')).toBe('multiple2 value');
                });
            });
        });

        describe('remove()', function () {
            it('should be defined', function () {
                expect(instance.remove).not.toBeUndefined();
            });

            it('should be a function', function () {
                expect(typeof instance.remove).toBe('function');
            });

            describe('returns', function () {
                it('the removed item', function () {
                    expect(instance.remove('one')).toBe('one value');
                });

                it('undefined when wants to get it', function () {
                    instance.remove('two');
                    expect(instance.get('two')).toBeUndefined();
                });
            });
        });

        describe('each()', function () {
            var callParams, instanceEach;

            beforeAll(function () {
                callParams   = [];
                instanceEach = new Hashtable({
                    one: 'one value',
                    two: 'two value'
                });

                function eachCallback(value, key) {
                    callParams.push([value, key]);
                };

                instanceEach.each(eachCallback);
            });

            it('should be defined', function () {
                expect(instanceEach.each).not.toBeUndefined();
            });

            it('should be a function', function () {
                expect(typeof instanceEach.each).toBe('function');
            });

            describe('should be called', function () {
                it('', function () {
                    expect(callParams.length > 0).toBe(true);
                });

                it('2 times', function () {
                    expect(callParams.length).toBe(2);
                });

                it('first with ["one value", "one"]', function () {
                    expect(callParams[0]).not.toBeUndefined();
                    expect(callParams[0][0]).toBe('one value');
                    expect(callParams[0][1]).toBe('one');
                });

                it('second with ["two value", "two"]', function () {
                    expect(callParams[1]).not.toBeUndefined();
                    expect(callParams[1][0]).toBe('two value');
                    expect(callParams[1][1]).toBe('two');
                });
            });
        });

        describe('sort()', function () {
            var expectedSortByKey  = ['Apple', 'Cancel', 'one', 'progress', 'two', 'Word', 'zion'],
                expectedSortByName = ['progress', 'one', 'two', 'Cancel', 'Apple', 'Word', 'zion'],
                instanceEach;

            instanceEach = new Hashtable({
                zion: {name: 'zion value'},
                Word: {name: 'Word value'},
                progress: {name: -22},
                Apple: {name: 'the apple value'},
                two: {name: 2},
                Cancel: {name: 'cancel value'},
                one: 1
            });



            it('should be defined', function () {
                expect(instanceEach.sort).not.toBeUndefined();
            });

            it('should be a function', function () {
                expect(typeof instanceEach.sort).toBe('function');
            });

            describe('by key', function () {
                it('should be as ["' + expectedSortByKey.join('", "')+'"]', function () {
                    var keys = [];

                    instanceEach.sort();
                    instanceEach.each(function (value, key) {
                        keys.push(key);
                    });
                    expect(keys).toEqual(expectedSortByKey);
                });
            });

            describe('by `name`', function () {
                it('should be as ["' + expectedSortByName.join('", "')+'"]', function () {
                    var keys = [];

                    instanceEach.sort('name');
                    instanceEach.each(function (value, key) {
                        keys.push(key);
                    });
                    expect(keys).toEqual(expectedSortByName);
                });
            });

        });
    });
});
