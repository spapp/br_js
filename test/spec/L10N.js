/**
 * @author    Sándor Papp <spapp@spappsite.hu>
 * @copyright 2017
 * @license   http://opensource.org/licenses/GPL-3.0 GNU General Public License v3
 * @package   BrJs
 * @since     2017-07-15
 */

describe('L10N', function () {

    it('should be defined', function () {
        expect(L10N).not.toBeUndefined();
    });

    it('should be an object', function () {
        expect(Object.prototype.toString.call(L10N)).toBe('[object Object]');
    });

    describe('LOCAL_EN', function () {
        it('should be defined', function () {
            expect(L10N.LOCAL_EN).not.toBeUndefined();
        });

        it('should be "en"', function () {
            expect(L10N.LOCAL_EN).toBe('en');
        });
    });

    describe('LOCAL_HU', function () {
        it('should be defined', function () {
            expect(L10N.LOCAL_HU).not.toBeUndefined();
        });

        it('should be "hu"', function () {
            expect(L10N.LOCAL_HU).toBe('hu');
        });
    });

    describe('isSupported()', function () {
        it('should be defined', function () {
            expect(L10N.isSupported).not.toBeUndefined();
        });

        it('should be a function', function () {
            expect(typeof L10N.isSupported).toBe('function');
        });

        it('should returns TRUE', function () {
            expect(L10N.isSupported('en')).toBe(true);
            expect(L10N.isSupported('hu')).toBe(true);
        });

        it('should returns FALSE', function () {
            expect(L10N.isSupported('gb')).toBe(false);
            expect(L10N.isSupported('xx')).toBe(false);
        });
    });

    describe('getLetterMap()', function () {
        it('should be defined', function () {
            expect(L10N.getLetterMap).not.toBeUndefined();
        });

        it('should be a function', function () {
            expect(typeof L10N.getLetterMap).toBe('function');
        });

        it('should returns an object', function () {
            expect(Object.prototype.toString.call(L10N.getLetterMap('en'))).toBe('[object Object]');
        });
    });

    describe('getDefaultLanguage()', function () {
        it('should be defined', function () {
            expect(L10N.getDefaultLanguage).not.toBeUndefined();
        });

        it('should be a function', function () {
            expect(typeof L10N.getDefaultLanguage).toBe('function');
        });

        it('should returns a string', function () {
            expect(typeof L10N.getDefaultLanguage()).toBe('string');
        });

        it('should returns a supported language', function () {
            expect(L10N.isSupported(L10N.getDefaultLanguage())).toBe(true);
        });
    });
});