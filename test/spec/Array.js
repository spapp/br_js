/**
 * @author    Sándor Papp <spapp@spappsite.hu>
 * @copyright 2017
 * @license   http://opensource.org/licenses/GPL-3.0 GNU General Public License v3
 * @package   BrJs
 * @since     2017-07-15
 */
describe('Array', function () {
    var i, list, lists;

    lists = [
        {
            haystack: [10, 1, 0, 34, 3, 48, 53, -2, -12, -22],
            asc: [-22, -12, -2, 0, 1, 3, 10, 34, 48, 53]
        }, {
            haystack: [
                'hideg', 'Tamás', 'cukor', 'acél', 'sokáig', 'gép', 'remény', 'szabad', 'Nagy', 'csók',
                'vásárol', 'nyúl', 'olasz', 'öröm', 'kettő'
            ],
            asc: [
                'acél', 'cukor', 'csók', 'gép', 'hideg', 'kettő', 'Nagy', 'nyúl', 'olasz', 'öröm', 'remény',
                'sokáig', 'szabad', 'Tamás', 'vásárol'
            ]
        }, {
            haystack: [
                'opera', 'Jácint', 'viola', 'Opera', 'jácint', 'szűcs', 'Viola', 'Szűcs'
            ],
            asc: [
                'jácint', 'Jácint', 'opera', 'Opera', 'szűcs', 'Szűcs', 'viola', 'Viola'
            ]
        }, {
            haystack: [
                'zúdul', 'cuppant', 'Zoltán', 'Csepel', 'Zsigmond', 'csata', 'zsalu',
                'cukor', 'zongora', 'cudar', 'csalit', 'zseni'
            ],
            asc: [
                'cudar', 'cukor', 'cuppant', 'csalit', 'csata', 'Csepel', 'Zoltán',
                'zongora', 'zúdul', 'zsalu', 'zseni', 'Zsigmond'
            ]
        }, {
            haystack: [
                'tünemény', 'Lontay', 'pirinyó', 'lombik', 'tüntet', 'pirít', 'lomb', 'tükör', 'tüzér',
                'lovagol', 'pirkad', 'Tünde', 'lom', 'pirinkó', 'Piroska'
            ],
            asc: [
                'lom', 'lomb', 'lombik', 'Lontay', 'lovagol', 'pirinkó', 'pirinyó', 'pirít', 'pirkad',
                'Piroska', 'tükör', 'Tünde', 'tünemény', 'tüntet', 'tüzér'
            ]
        }, {
            haystack: [
                'meny', 'nagyít', 'menü', 'nagy', 'kaszt', 'Kassák', 'kaszinó', 'nagyobb', 'mennyi', 'kas',
                'kastély', 'Menyhért', 'naggyal', 'kassza', 'nagygyakorlat', 'mennek',
                'naggyá', 'menza', 'nagyol', 'kasza', 'mennybolt', 'Kasmír', 'nagyoll', 'mennének'
            ],
            asc: [
                'kas', 'Kasmír', 'Kassák', 'kastély', 'kasza', 'kaszinó', 'kassza', 'kaszt', 'mennek',
                'mennének', 'menü', 'menza', 'meny', 'Menyhért', 'mennybolt', 'mennyi', 'nagy',
                'naggyá', 'nagygyakorlat', 'naggyal', 'nagyít', 'nagyobb', 'nagyol', 'nagyoll'
            ]
        }, {
            haystack: [
                'író', 'iram', 'irónia', 'ír', 'írandó', 'Irak', 'iroda', 'Irán', 'iránt'
            ],
            asc: [
                'ír', 'Irak', 'iram', 'Irán', 'írandó', 'iránt', 'író', 'iroda', 'irónia'
            ]
        }, {
            // 14. c.
            //     haystack: [
            //         'szel', 'egyféle', 'szeles', 'Eger', 'elöl', 'szél', 'szüret', 'kerek', 'szűret', 'egyfelé', 'koros',
            //         'kerék', 'elől', 'kóros', 'egér', 'keres', 'kérés', 'széles'
            //     ],
            //     asc: [
            //         'Eger', 'egér', 'egyfelé', 'egyféle', 'elöl', 'elől', 'kerek', 'kerék', 'keres',
            //         'kérés', 'koros', 'kóros', 'szel', 'szél', 'szeles', 'széles', 'szüret', 'szűret'
            //     ]
            // }, {
            haystack: [
                'kis sorozat', 'kis virág', 'Tisza-part', 'tiszt', 'márvány', 'másol', 'kissorozat-gyártás',
                'márványkő', 'tiszafa', 'Tisza Kálmán', 'Kiss Ernő', 'Tisza menti', 'Márvány-tenger', 'kis részben',
                'másféle', 'márványtömb', 'kissé', 'kis számban', 'kistányér', 'márvány sírkő', 'Márvány Zsolt',
                'Tiszahát', 'Tiszántúl', 'tiszavirág'
            ],
            asc: [
                'kis részben', 'kissé', 'Kiss Ernő', 'kis sorozat', 'kissorozat-gyártás', 'kis számban',
                'kistányér', 'kis virág', 'márvány', 'márványkő', 'márvány sírkő', 'Márvány-tenger',
                'márványtömb', 'Márvány Zsolt', 'másféle', 'másol', 'tiszafa', 'Tiszahát', 'Tisza Kálmán',
                'Tisza menti', 'Tiszántúl', 'Tisza-part', 'tiszavirág', 'tiszt'
            ]
        }, {
            haystack: [
                'opera', 5, 'Jácint', 1, 'viola', 'Opera', 54, 'jácint', 2, 'szűcs', 'Viola', 'Szűcs', 12
            ],
            asc: [
                1, 2, 5, 12, 54, 'jácint', 'Jácint', 'opera', 'Opera', 'szűcs', 'Szűcs', 'viola', 'Viola'
            ]
        }, {
            haystack: [
                {name: 'opera'},
                {name: 5},
                'Jácint',
                1,
                {name: 'viola'},
                {name: {name: 'Opera'}},
                {name: 54},
                {name: 'jácint'},
                {name: 2},
                {name: 'szűcs'},
                {name: 'Viola'},
                'Szűcs',
                {name: 12}
            ],
            asc: [
                1,
                {name: 2},
                {name: 5},
                {name: 12},
                {name: 54},
                {name: 'jácint'},
                'Jácint',
                {name: 'opera'},
                {name: {name: 'Opera'}},
                {name: 'szűcs'},
                'Szűcs',
                {name: 'viola'},
                {name: 'Viola'}
            ]
        }
    ];

    function sortTest(haystack, expectedList, direction, local, propertyName) {
        describe('and the direction is ' + direction, function () {
            it('should be ' + JSON.stringify(expectedList), function () {
                expect(Array.sort(haystack, direction, local, propertyName)).toEqual(expectedList);
            });
        });
    }

    describe('static', function () {
        describe('DIRECTION_ASC', function () {
            it('should be defined', function () {
                expect(Array.DIRECTION_ASC).not.toBeUndefined();
            });

            it('should be a string', function () {
                expect(typeof Array.DIRECTION_ASC).toBe('string');
            });

            it('should be "asc"', function () {
                expect(Array.DIRECTION_ASC).toBe('asc');
            });
        });

        describe('DIRECTION_DESC', function () {
            it('should be defined', function () {
                expect(Array.DIRECTION_DESC).not.toBeUndefined();
            });

            it('should be a string', function () {
                expect(typeof Array.DIRECTION_DESC).toBe('string');
            });

            it('should be "desc"', function () {
                expect(Array.DIRECTION_DESC).toBe('desc');
            });
        });

        describe('sort()', function () {
            it('should be defined', function () {
                expect(Array.sort).not.toBeUndefined();
            });

            it('should be a function', function () {
                expect(typeof Array.sort).toBe('function');
            });

            describe('when the local is ' + L10N.LOCAL_HU, function () {
                for (i = 0; list = lists[i]; i++) {
                    sortTest(list.haystack, list.asc, Array.DIRECTION_ASC, L10N.LOCAL_HU, 'name');
                    sortTest(list.haystack, [].concat(list.asc).reverse(), Array.DIRECTION_DESC, L10N.LOCAL_HU, 'name');
                }
            });
        });
    });
});
